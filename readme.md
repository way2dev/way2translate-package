# Way2Translate

**Disclaimer**: Way2Translate is an internal package used to manage translations in Laravel. We offer no support on this package, but if you can use it, feel free to require it in your composer.json.

## Installation:

- Add the repository to your composer.json:

  ```
  "repositories": [
      {
          "type": "hg",
          "url": "https://bitbucket.org/way2dev/laravel-way2translate"
      }
  ]
  ```

- Add the require to your composer.json:

  ```
  "way2web/laravel-way2translate": "2.2.*"
  ```

- Update composer: `$ composer update`

- Remove or comment out the default TranslationServiceProvider:

  ```
  Illuminate\Translation\TranslationServiceProvider::class,
  ```

- Add our provider:

  ```
  Way2Web\Way2Translate\Providers\Way2TranslateProvider::class,
  ```

- Activate all the locales in `config/laravellocalization.php`, Way2Translate will use this list as a source to add new translations.

- Publish the config and migrations: `php artisan vendor:publish`

- Execute the migrations: `php artisan migrate`

- Run the `w2w:import-translations` command to import the English translations to the database.

- Add the navigation link to the navbar for administrators:

  ```
  <li>
    <a href="{{ route('way2translate.index') }}">
        <i class="fa fa-btn fa-flag"></i>
        {{ trans('way2translate::page.manage-translations') }}
    </a>
  </li>
  ```

## Translations to Javascript

- If you use a build system, ensure the `artisan w2w:export-translations-js` is part of the build process. This command is needed to generate the lang.js file, used to retrieve translations.
- In the way2translate config, set "js-translations.auto-generate" to "true" if you would like the lang.js file to be automatically regenerated upon a translation change. Otherwise, feel free to listen to the `Way2Web\Way2Translate\Events\ModifiedTranslations` event, and perform your own logic.
- Include the lang.js file in the footer. A url to the lang.js file can be generated (including a modified timestamp for cachebusting) via: `Way2Web\Way2Translate\Helpers\Js::getLangUrl()`.

## Overriding views

You can override the included views by creating a `vendor/way2translate` folder in your `resources/views` folder. In this folder you can add all the views/folders you wish to override from the package. To override, for example, the master view, you can add `resources/views/vendor/way2translate/master.blade.php` and that file will now be used.

## Language dropdown

If you want a dropdown with all available locales, you can, for example, use the following code:

```
@if(count(\Way2Web\Way2Translate\Models\Locale::getActive()) > 1)
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ trans('localisation.language') }} <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            @foreach(\Way2Web\Way2Translate\Models\Locale::getActive()->sortBy('native') as $locale)
                <li
                    @if (LaravelLocalization::getCurrentLocale() == $locale['code'])
                        class="active"
                    @endif
                >
                    <a rel="alternate" hreflang="{{$locale['code']}}" href="{{LaravelLocalization::getLocalizedURL($locale['code']) }}">
                        {{ $locale['native'] }}
                    </a>
                </li>
            @endforeach
        </ul>
    </li>
@endif
```

**You should really place the Locale model calls in a view provider!**

## Styling:

Out-of-the-box there must be a master layout: `views/layouts/admin/app.blade.php`

You can however change this behaviour by overriding the `master.blade.php`

# Local development:

If you want to work on the package locally, you can set up the repositories in your composer.json like so:

```
"repositories": [
    {
        "type": "path",
        "url": "/local/directory/laravel-way2translate/",
        "options": {
            "symlink": true
        }
    }
]
```

And require the package like this:

```
"way2web/laravel-way2translate": "@dev"
```

Followed by a composer update: `$ composer update way2web/laravel-way2translate`.

## Special thanks:

- [Laravel-JS-Localization](https://github.com/rmariuzzo/Laravel-JS-Localization)
- [JS-Lang](https://github.com/rmariuzzo/lang.js)
