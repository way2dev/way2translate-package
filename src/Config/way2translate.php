<?php

return [
    'default-locale'       => 'en',
    'import-locale'        => 'en',
    'locales'              => [],
    'cache-duration'       => 30,
    'register-breadcrumbs' => true,
    'editable-languages'   => true,

    'js-translations' => [
        'auto-generate'   => true,
        'include-lang-js' => true,
        'minify-js'       => true,
        'path-relative'   => 'dist/lang.js',
        'path-absolute'   => public_path('dist/lang.js'),
    ],

    'theme' => [
        'name'           => 'bootstrap-3',
        'master-extends' => 'layouts.admin.app',
        'master-section' => 'content',

        'classes' => [
            'form' => '',

            'label' => 'control-label',

            'static' => 'form-control-static',

            'select'   => 'form-control',
            'textarea' => 'form-control',
            'input'    => 'form-control',
        ],
    ],
];
