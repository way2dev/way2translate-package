<?php

namespace Way2Web\Way2Translate\Console\Commands;

use Illuminate\Console\Command;
use Way2Web\Way2Translate\Generators\JsGenerator;
use Way2Web\Way2Translate\Models\Translation;

/**
 * Exports the translations to a javascript file.
 */
class ExportTranslationsJsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'w2w:export-translations-js';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exports the translations to a javascript file.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fromDatabase = Translation::hasDefaultTranslations();

        if ($fromDatabase) {
            $this->info('Exporting translations from the database.');
        } else {
            $this->info('Exporting translations from the filesystem.');
        }

        $generator = new JsGenerator();
        $generator->generate($fromDatabase);

        $this->info(
            'Export finished. "' . config('way2translate.js-translations.path-relative') . '" has been generated.'
        );
    }
}
