<?php

namespace Way2Web\Way2Translate\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Way2Web\Way2Translate\Models\Language;
use Way2Web\Way2Translate\Models\Locale;
use Way2Web\Way2Translate\Models\Translation;

/**
 * Import the English translations into the database.
 *
 * Heavily inspired by:
 * https://github.com/hpolthof/laravel-translations-db/blob/master/src/Console/Commands/FetchCommand.php
 */
class ImportTranslationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'w2w:import-translations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the English translations from the filesystem to the database';

    /**
     * The default language to import.
     *
     * @var string
     */
    private $importLocale;

    /**
     * The default lang folder.
     *
     * @var string
     */
    private $langPath;

    /**
     * All current translated locales.
     *
     * @var array
     */
    private $allTranslationLocales;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->importLocale = Config::get('way2translate.import-locale');
        $this->langPath = resource_path('lang');

        $languages = Language::get()->pluck('locale')->toArray();
        if (!in_array($this->importLocale, $languages)) {
            $languages[] = $this->importLocale;
        }

        $this->allTranslationLocales = $languages;

        $this->setInitialInLatestImportState();

        $this->loadDirectory($this->langPath . '/' . $this->importLocale);

        $this->clearNotInLatestImport();

        Locale::clearCache();

        $this->info('Import done');
    }

    /**
     * Set the in latest import to false, so at the end, we can now which translations are no longer used.
     */
    private function setInitialInLatestImportState()
    {
        Translation::where('id', '>', '0')->update(['in_latest_import' => false]);
    }

    /**
     * Delete the unused translations.
     */
    private function clearNotInLatestImport()
    {
        Translation::where('in_latest_import', false)->delete();
    }

    /**
     * Load a directory and process the subdirectories and files.
     *
     * @param string $path
     */
    private function loadDirectory($path)
    {
        $groups = $this->getGroups($path);
        foreach ($groups as $group) {
            $this->processGroup($path, $group);
        }
    }

    /**
     * Process the group file for storage into the database.
     *
     * @param string $path
     * @param string $group
     */
    private function processGroup($path, $group)
    {
        $keys = require $path . '/' . $group . '.php';
        $keys = $this->flattenArray($keys);
        foreach ($keys as $name => $value) {
            $this->storeTranslation($group, $name, $value);
        }

        Translation::clearGroupCache($this->importLocale, $group);

        $this->info('Processed ' . $group . '.php');
    }

    /**
     * Store the translation in the database, but only if it not already exists.
     *
     * @param string $group
     * @param string $name
     * @param string $value
     */
    private function storeTranslation($group, $name, $value)
    {
        // ensure this new translation is added to all languages
        foreach ($this->allTranslationLocales as $locale) {
            if (!Translation::missingTranslation($locale, $group, $name)) {
                continue;
            }

            $translation = new Translation();
            $translation->locale = $locale;
            $translation->group = $group;
            $translation->name = $name;

            if ($locale != $this->importLocale) {
                $translation->value = '';
            } else {
                $translation->value = $value;
            }

            $translation->in_latest_import = true;
            $translation->save();

            $this->info('Imported ' . $locale . ':' . $group . ':' . $name . ' translation.');
        }

        // mark the translation as imported
        Translation::where('group', $group)->where('name', $name)->update(['in_latest_import' => true]);

        return true;
    }

    /**
     * Flatten the multidimensional arrays to strings.
     *
     * @param array  $keys
     * @param string $prefix
     *
     * @return array
     */
    protected function flattenArray($keys, $prefix = '')
    {
        $result = [];
        foreach ($keys as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->flattenArray($value, $prefix . $key . '.'));
            } else {
                $result[$prefix . $key] = $value;
            }
        }

        return $result;
    }

    /**
     * Get the groups from the directory and run them through a cleaning function.
     *
     * @param string $path
     *
     * @return array
     */
    protected function getGroups($path)
    {
        $groups = File::files($path);
        foreach ($groups as &$group) {
            $group = $this->cleanGroupDir($group, $path);
        }

        return $groups;
    }

    /**
     * Removes the path and extension from the group.
     *
     * @param string $group
     * @param string $path
     *
     * @return string
     */
    protected function cleanGroupDir($group, $path)
    {
        $clean = str_replace($path . '/', '', $group);
        if (preg_match('/^(.*?)\.php$/sm', $clean, $match)) {
            return $match[1];
        }

        return false;
    }
}
