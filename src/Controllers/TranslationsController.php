<?php

namespace Way2Web\Way2Translate\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Way2Web\Way2Translate\Events\ModifiedTranslations;
use Way2Web\Way2Translate\Models\Language;
use Way2Web\Way2Translate\Models\Locale;
use Way2Web\Way2Translate\Models\Translation;

/**
 * Manage translations.
 */
class TranslationsController extends BaseController
{
    /**
     * Manage translations.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (!Translation::hasDefaultTranslations()) {
            return redirect()->route('way2translate.missing-translations');
        }

        return view('way2translate::index', [
            'translatedLocales'          => Locale::getTranslated(),
            'nonTranslatedLocalesTarget' => Locale::getNonTranslated()->pluck('name', 'code'),
        ]);
    }

    /**
     * Missing translations.
     *
     * @return \Illuminate\View\View
     */
    public function missingTranslations()
    {
        return view('way2translate::missing-translations');
    }

    /**
     * Activate a locale.
     *
     * @param string $localeCode
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($localeCode)
    {
        Language::activate($localeCode);

        Locale::clearCache();

        return redirect()->route('way2translate.index');
    }

    /**
     * Deactivate a locale.
     *
     * @param string $localeCode
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate($localeCode)
    {
        if ($localeCode != Config::get('way2translate.default-locale')) {
            Language::deactivate($localeCode);

            Locale::clearCache();
        }

        return redirect()->route('way2translate.index');
    }

    /**
     * Creates a new translation based on the source locale.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request)
    {
        $nonTranslatedLocales = Locale::getNonTranslated()->pluck('code')->toArray();

        $this->validate($request, [
            'locale_target' => 'required|in:' . implode(',', $nonTranslatedLocales),
        ]);

        $localeTarget = $request->get('locale_target');

        $translations = Translation::where('locale', Config::get('way2translate.default-locale'))->get();
        if (empty($translations)) {
            return redirect()->route('way2translate.index');
        }

        foreach ($translations as $translation) {
            $newTranslation = $translation->replicate();
            $newTranslation->locale = $localeTarget;
            $newTranslation->value = '';
            $newTranslation->save();
        }

        Language::addByLocale($localeTarget);

        Locale::clearCache();

        return redirect()->route('way2translate.index');
    }

    /**
     * Manage the group translations.
     *
     * @param string $localeCode
     * @param string $group
     *
     * @return \Illuminate\View\View
     */
    public function group($localeCode, $group = '')
    {
        $locale = Locale::getByCode($localeCode);
        if (empty($locale)) {
            abort(404);
        }

        if (empty($group)) {
            $translation = Translation::where('locale', $localeCode)->first();

            return redirect()->route('way2translate.group.index', [$localeCode, $translation->group]);
        }

        return view('way2translate::group', [
            'locale'       => $locale,
            'group'        => $group,
            'groups'       => Translation::getGroups(),
            'translations' => Translation::getGroupTranslations($localeCode, $group),
        ]);
    }

    /**
     * Manage the group translations.
     *
     * @param Request $request
     * @param string  $localeCode
     * @param string  $group
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function groupSave(Request $request, $localeCode, $group)
    {
        if (!empty($request->get('value'))) {
            foreach ($request->get('value') as $id => $value) {
                Translation::where('id', $id)->update(['value' => $value]);
            }
        }

        Translation::clearGroupCache($localeCode, $group);

        Event::dispatch(new ModifiedTranslations($localeCode, $group));

        Locale::clearCache();

        if (config('way2translate.js-translations.auto-generate')) {
            Artisan::call('w2w:export-translations-js');
        }

        return redirect()->route('way2translate.group.index', [$localeCode, $group]);
    }
}
