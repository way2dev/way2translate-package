<?php

namespace Way2Web\Way2Translate\Events;

use Illuminate\Queue\SerializesModels;

/**
 * Activate the language.
 */
class ActivateLanguage extends Event
{
    use SerializesModels;

    /** @var string */
    public $localeCode;

    /**
     * Create a new event instance.
     */
    public function __construct($localeCode)
    {
        $this->localeCode = $localeCode;
    }
}
