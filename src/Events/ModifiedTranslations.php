<?php

namespace Way2Web\Way2Translate\Events;

use Illuminate\Queue\SerializesModels;

/**
 * Modified a translation.
 */
class ModifiedTranslations extends Event
{
    use SerializesModels;

    /** @var string */
    public $localeCode;

    /** @var string */
    public $group;

    /**
     * Create a new event instance.
     *
     * @param string $localeCode
     * @param string $group
     */
    public function __construct($localeCode, $group)
    {
        $this->localeCode = $localeCode;
        $this->group = $group;
    }
}
