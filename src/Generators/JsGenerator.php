<?php

namespace Way2Web\Way2Translate\Generators;

use Exception;
use Illuminate\Filesystem\Filesystem as File;
use JShrink\Minifier;
use Way2Web\Way2Translate\Models\Translation;

/**
 * The JsGenerator class.
 */
class JsGenerator
{
    /**
     * The file service.
     *
     * @var File
     */
    protected $file;

    /**
     * Construct a new JsGenerator instance.
     */
    public function __construct()
    {
        $this->file = app('files');
    }

    /**
     * Generate a javascript language file.
     *
     * @param bool $fromDatabase
     *
     * @return int
     */
    public function generate($fromDatabase = true)
    {
        if ($fromDatabase) {
            $messages = $this->getMessagesFromDatabase();
        } else {
            $messages = $this->getMessagesFromFilesystem();
        }

        $target = config('way2translate.js-translations.path-absolute');
        $this->prepareTarget($target);

        $template = $this->file->get(__DIR__ . '/Templates/langjs-with-messages.js');

        $langjs = '';
        if (config('way2translate.js-translations.include-lang-js')) {
            $langjs = $this->file->get(__DIR__ . '/Lib/lang.min.js');
        }

        $template = str_replace('\'{ langjs }\';', $langjs, $template);
        $template = str_replace('\'{ messages }\'', json_encode($messages), $template);

        if (config('way2translate.js-translations.minify-js')) {
            $template = Minifier::minify($template);
        }

        return $this->file->put($target, $template);
    }

    /**
     * Return all language messages from the database.
     *
     * @return array
     */
    protected function getMessagesFromDatabase()
    {
        $messages = [];

        $translations = Translation::all();
        $locales = $translations->unique('locale')->pluck('locale');
        $groups = $translations->unique('group')->pluck('group');

        foreach ($locales as $locale) {
            foreach ($groups as $group) {
                $groupKey = $locale . '.' . $group;
                $groupTranslations = $translations->where('locale', $locale)->where('group', $group);

                $groupMessages = [];

                $keyTranslations = $groupTranslations->pluck('value', 'name')->toArray();
                foreach ($keyTranslations as $key => $value) {
                    array_set($groupMessages, $key, $value);
                }

                $messages[$groupKey] = $groupMessages;
            }
        }

        $this->sortMessages($messages);

        return $messages;
    }

    /**
     * Return all language messages from the filesystem.
     *
     * @return array
     *
     * @throws Exception
     */
    protected function getMessagesFromFilesystem()
    {
        $messages = [];

        $importLocale = config('way2translate.import-locale');

        $messages = [];
        $path = resource_path('lang' . DIRECTORY_SEPARATOR . $importLocale);
        if (!$this->file->exists($path)) {
            throw new Exception($path . 'does not exists!');
        }

        foreach ($this->file->allFiles($path) as $file) {
            $pathName = $file->getRelativePathName();
            if ($this->file->extension($pathName) !== 'php') {
                continue;
            }

            $key = $this->getKeyFromPathName($pathName);

            $messages[$importLocale . '.' . $key] = include $path . DIRECTORY_SEPARATOR . $pathName;
        }

        $this->sortMessages($messages);

        return $messages;
    }

    /**
     * Returns the key from the path name.
     *
     * @param string $pathName
     *
     * @return string
     */
    protected function getKeyFromPathName($pathName)
    {
        $key = substr($pathName, 0, -4);
        $key = str_replace('\\', '.', $key);
        $key = str_replace('/', '.', $key);

        if (starts_with($key, 'vendor')) {
            $key = $this->getVendorKey($key);
        }

        return $key;
    }

    /**
     * Prepare the target directory.
     *
     * @param string $target the target directory
     */
    protected function prepareTarget($target)
    {
        $dirname = dirname($target);

        if (!$this->file->exists($dirname)) {
            $this->file->makeDirectory($dirname, null, true);
        }
    }

    /**
     * Recursively sorts all messages by key.
     *
     * @param array $messages the messages to sort by key
     */
    protected function sortMessages(&$messages)
    {
        if (is_array($messages)) {
            ksort($messages);

            foreach ($messages as &$value) {
                $this->sortMessages($value);
            }
        }
    }

    /**
     * Returns the vendor key.
     *
     * @param string $key
     *
     * @return string
     */
    private function getVendorKey($key)
    {
        $keyParts = explode('.', $key, 4);
        unset($keyParts[0]);

        return $keyParts[2] . '.' . $keyParts[1] . '::' . $keyParts[3];
    }
}
