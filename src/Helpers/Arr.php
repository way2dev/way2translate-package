<?php

namespace Way2Web\Way2Translate\Helpers;

/**
 * Array helpers. Since it's impossible to name the class Array, we will name it Arr, like Laravel.
 */
class Arr
{
    /**
     * Transforms nested, dotted, arrays to a regular arrays.
     *
     * @param array
     *
     * @return array
     */
    public static function undot(array $dottedArray)
    {
        $unDotted = [];
        foreach ($dottedArray as $key => $value) {
            array_set($unDotted, $key, $value);
        }

        return $unDotted;
    }
}
