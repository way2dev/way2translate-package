<?php

namespace Way2Web\Way2Translate\Helpers;

/**
 * Helper javascript.
 */
class Js
{
    /**
     * Returns the url to the language file including it's modified timestamp.
     *
     * @return string
     */
    public static function getLangUrl()
    {
        $filePath = config('way2translate.js-translations.path-absolute');
        $fileRelative = config('way2translate.js-translations.path-relative');

        $langFileModified = '';
        if (app('files')->exists($filePath)) {
            $langFileModified = app('files')->lastModified($filePath);
        }

        $assetUrl = asset($fileRelative) . '?t=' . $langFileModified;

        return $assetUrl;
    }
}
