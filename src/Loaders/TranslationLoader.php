<?php

namespace Way2Web\Way2Translate\Loaders;

use Illuminate\Translation\FileLoader;
use Way2Web\Way2Translate\Models\Locale;
use Way2Web\Way2Translate\Models\Translation;

/**
 * Our custom translation loader.
 */
class TranslationLoader extends FileLoader
{
    /**
     * Load the translations for the given locale.
     *
     * @param string $locale
     * @param string $group
     * @param string $namespace
     *
     * @return array
     */
    public function load($locale, $group, $namespace = null)
    {
        // if there is a namespaced translation requested, fall back to the default loader
        if ($namespace !== null && $namespace !== '*') {
            return $this->loadNamespaced($locale, $group, $namespace);
        }

        // if no translations are present in the database, fall back to the default loader
        $translatedLocales = Locale::getTranslated();
        if ($translatedLocales->isEmpty()) {
            return FileLoader::load($locale, $group, $namespace);
        }

        // retrieve the translations from our model
        return Translation::getGroupTranslationsForLoader($locale, $group);
    }
}
