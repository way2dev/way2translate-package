<?php

namespace Way2Web\Way2Translate\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;
use Way2Web\Way2Translate\Models\Locale;
use Way2Web\Way2Translate\Models\Translation;

/**
 * If languages are not editable, the language management routes should not be available.
 */
class NonEditableLanguages
{
    /**
     * Check for missing translations first, then whether the languages are editable.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure                  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Translation::hasDefaultTranslations()) {
            return redirect()->route('way2translate.missing-translations');
        }

        if (!Config::get('way2translate.editable-languages')) {
            return redirect()->route('way2translate.group.index', Locale::getDefaultLanguageCode());
        }

        return $next($request);
    }
}
