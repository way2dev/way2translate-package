<?php

namespace Way2Web\Way2Translate\Models;

use Carbon\Carbon;
use Event;
use Illuminate\Database\Eloquent\Model;
use Way2Web\Way2Translate\Events\ActivateLanguage;

/**
 * Defines a language.
 */
class Language extends Model
{
    /** @var string */
    protected $table = 'way2translate_languages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'locale',
    ];

    /** @var array */
    protected $dates = [
        'activated_at',
        'created_at',
        'updated_at',
    ];

    /**
     * Activate a language.
     *
     * @param string $localeCode
     */
    public static function activate($localeCode)
    {
        if (self::where('locale', $localeCode)->get()->isEmpty()) {
            self::addByLocale($localeCode);
        }

        self::where('locale', $localeCode)->update([
            'activated_at' => Carbon::now(),
        ]);

        Event::dispatch(new ActivateLanguage($localeCode));
    }

    /**
     * Deactivate a language.
     *
     * @param string $localeCode
     */
    public static function deactivate($localeCode)
    {
        self::where('locale', $localeCode)->update([
            'activated_at' => null,
        ]);
    }

    /**
     * Get all active languages.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getActive()
    {
        return self::whereNotNull('activated_at')->get();
    }

    /**
     * Add a language by locale code.
     *
     * @param string $localeCode
     */
    public static function addByLocale($localeCode)
    {
        self::create([
            'locale'       => $localeCode,
            'activated_at' => null,
        ]);
    }
}
