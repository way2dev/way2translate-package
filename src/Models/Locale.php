<?php

namespace Way2Web\Way2Translate\Models;

use Cache;
use Illuminate\Support\Facades\Config;

/**
 * Defines an active locale.
 */
class Locale
{
    /**
     * Get all the locales with an active state.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function get()
    {
        return Cache::remember(
            self::getCacheKey(),
            Config::get('way2translate.cache-duration'),
            function () {
                $activeLanguages = Language::getActive();
                $translatedLanguages = Translation::getTranslatedLanguages()->toArray();

                $allLocales = [];
                foreach (Config::get('way2translate.locales') as $code => $locale) {
                    $locale['code'] = $code;
                    $locale['active'] = false;
                    $locale['is_translated'] = false;
                    $locale['translated_percent'] = 0;

                    if (!empty($activeLanguages->where('locale', $code)->toArray())) {
                        $locale['active'] = true;
                    }

                    if (in_array($code, $translatedLanguages)) {
                        $locale['is_translated'] = true;
                        $locale['translated_percent'] = Translation::getPercentCompleted($code);
                    }

                    $allLocales[] = $locale;
                }

                return collect($allLocales)->sortBy('name');
            }
        );
    }

    /**
     * Clears the locale cache.
     */
    public static function clearCache()
    {
        Cache::forget(self::getCacheKey());
    }

    /**
     * Get the cache key for locales.
     *
     * @return string
     */
    public static function getCacheKey()
    {
        return 'way2translate.active.locales';
    }

    /**
     * Get a locale by code.
     *
     * @param string $code
     *
     * @return array
     */
    public static function getByCode($code)
    {
        $allLocales = self::get();

        return $allLocales->where('code', $code)->first();
    }

    /**
     * Get all active locales.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getActive()
    {
        $allLocales = self::get();

        return $allLocales->where('active', true);
    }

    /**
     * Get the configured default language code, otherwise return the code of the first language in the database.
     *
     * @return string
     */
    public static function getDefaultLanguageCode()
    {
        if (Config::get('way2translate.default-locale')) {
            return Config::get('way2translate.default-locale');
        }

        return self::getActive()->first()['code'];
    }

    /**
     * Get all translated locales.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getTranslated()
    {
        $allLocales = self::get();

        return $allLocales->where('is_translated', true);
    }

    /**
     * Get all non-translated locales.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getNonTranslated()
    {
        $allLocales = self::get();

        return $allLocales->where('is_translated', false);
    }
}
