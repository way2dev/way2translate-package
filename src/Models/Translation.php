<?php

namespace Way2Web\Way2Translate\Models;

use Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Way2Web\Way2Translate\Helpers\Arr;

/**
 * Defines a translation.
 */
class Translation extends Model
{
    /** @var string */
    protected $table = 'way2translate_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'locale',
        'group',
        'prefix',
        'key',
        'value',
        'in_latest_import',
    ];

    /** @var array */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get all unique locales that have been translated.
     *
     * @return array
     */
    public static function getTranslatedLanguages()
    {
        return self::get()->unique('locale')->pluck('locale');
    }

    /**
     * Check if the translation is missing.
     *
     * @param string $locale
     * @param string $group
     * @param string $name
     *
     * @return bool
     */
    public static function missingTranslation($locale, $group, $name)
    {
        return self::where('locale', $locale)->where('group', $group)->where('name', $name)->get()->isEmpty();
    }

    /**
     * Get the group translations.
     *
     * @param string $locale
     * @param string $group
     *
     * @return \Way2Web\Way2Translate\Models\Translation
     */
    public static function getGroupTranslations($locale, $group)
    {
        return self::where('locale', $locale)->where('group', $group)->orderBy('name', 'asc')->get();
    }

    /**
     * Get all unique groups that have been translated.
     *
     * @return array
     */
    public static function getGroups()
    {
        return self::get()->unique('group')->sortBy('group')->pluck('group');
    }

    /**
     * Retrieves the translations for the loader.
     *
     * @param string $locale
     * @param string $group
     *
     * @return array
     */
    public static function getGroupTranslationsForLoader($locale, $group)
    {
        return Cache::remember(
            self::getCacheKey($locale, $group),
            Config::get('way2translate.cache-duration'),
            function () use ($group, $locale) {
                $translations = self::getGroupTranslations($locale, $group)->pluck('value', 'name')->toArray();

                return Arr::undot($translations);
            }
        );
    }

    /**
     * Clears the cache for a given locale and group combination.
     *
     * @param string $locale
     * @param string $group
     */
    public static function clearGroupCache($locale, $group)
    {
        Cache::forget(self::getCacheKey($locale, $group));
    }

    /**
     * Get the cache key for a given locale and group combination.
     *
     * @param string $locale
     * @param string $group
     *
     * @return string
     */
    public static function getCacheKey($locale, $group)
    {
        return 'way2translate.translations.' . $locale . '.' . $group;
    }

    /**
     * Check if the default translations are present.
     *
     * @return bool
     */
    public static function hasDefaultTranslations()
    {
        return !self::where('locale', Config::get('way2translate.default-locale'))->get()->isEmpty();
    }

    /**
     * Get the completion percentage of the given locale.
     *
     * @param string $localeCode
     *
     * @return float
     */
    public static function getPercentCompleted($localeCode)
    {
        $allTranslations = self::where('locale', $localeCode)->get();
        $emptyTranslations = $allTranslations->where('value', '');

        $percentEmpty = ($emptyTranslations->count() / $allTranslations->count()) * 100;
        $percentFilled = 100 - $percentEmpty;

        return round($percentFilled, 2);
    }
}
