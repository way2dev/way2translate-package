<?php

namespace Way2Web\Way2Translate\Providers;

use Illuminate\Translation\TranslationServiceProvider as ServiceProvider;
use Way2Web\Way2Translate\Loaders\TranslationLoader;

/**
 * Our custom translation service provider.
 */
class TranslationServiceProvider extends ServiceProvider
{
    /**
     * Register our loader.
     */
    protected function registerLoader()
    {
        $this->app->singleton('translation.loader', function ($app) {
            return new TranslationLoader($app['files'], $app['path.lang']);
        });
    }
}
