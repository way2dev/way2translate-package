<?php

namespace Way2Web\Way2Translate\Services;

use Illuminate\Support\Facades\Log;
use Way2Web\Way2Translate\Models\Language;
use Way2Web\Way2Translate\Models\Locale;

/**
 * Handles cache logic.
 */
class CacheService
{
    /**
     * Checks the integrity of the translation cache. If there is an inconsistency, it will be purged.
     */
    public function enforceCacheIntegrity()
    {
        $cachedLocales = Locale::get();
        $activeLanguage = Language::getActive();

        $shouldHaveCache = $activeLanguage->isNotEmpty();
        $hasCache = $cachedLocales->isNotEmpty();

        if ($shouldHaveCache && !$hasCache) {
            Log::error('Way2Translate: the translation cache is empty when it should not have been');

            $this->refreshLocaleCache();
        }
    }

    /**
     * Refreshes the locale cache.
     */
    private function refreshLocaleCache()
    {
        Locale::clearCache();

        Locale::get();
    }
}
