<?php

return [
    'add-language' => 'Add language',
    'activate'     => 'Activate',
    'back'         => 'Back',
    'create'       => 'Create',
    'deactivate'   => 'Deactivate',
    'edit'         => 'Edit',
    'store'        => 'Save',
];
