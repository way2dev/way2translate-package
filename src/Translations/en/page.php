<?php

return [
    'manage-translations'         => 'Manage translations',
    'missing-translations'        => 'Missing translations',
    'group-translations'          => 'Group translations',
    'group-translations-detailed' => ':locale :group translations',
];
