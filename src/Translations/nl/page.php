<?php

return [
    'manage-translations'         => 'Beheer vertalingen',
    'missing-translations'        => 'Vertalingen ontbreken',
    'group-translations'          => 'Groep vertalingen',
    'group-translations-detailed' => ':locale :group vertalingen',
];
