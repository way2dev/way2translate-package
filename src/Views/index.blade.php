@extends($themePath . '.pages.index')

@section('title', trans('way2translate::page.manage-translations'))
@section('heading', trans('way2translate::page.manage-translations'))

@section('translations-heading', trans('way2translate::translation.translations'))

@section('translations-table')
    @component($themeComponentPath . '.table.table')
        @component($themeComponentPath . '.table.thead') 
            @component($themeComponentPath . '.table.thead') 
                @component($themeComponentPath . '.table.th') 
                    {{ trans('way2translate::translation.language') }}
                @endcomponent
                
                @component($themeComponentPath . '.table.th') 
                    {{ trans('way2translate::translation.translated_percent') }}
                @endcomponent

                @component($themeComponentPath . '.table.th') 
                    {{ trans('way2translate::translation.status') }}
                @endcomponent

                @component($themeComponentPath . '.table.th') 
                @endcomponent
            @endcomponent
        @endcomponent
    
        @component($themeComponentPath . '.table.tbody') 
            @foreach ($translatedLocales as $index => $translatedLocale)
                @component($themeComponentPath . '.table.row')
                    @component($themeComponentPath . '.table.td')
                        {{ $translatedLocale['name'] }}
                    @endcomponent
                    
                    @component($themeComponentPath . '.table.td')
                        {{ $translatedLocale['translated_percent'] . '%' }}
                    @endcomponent
                    
                    @component($themeComponentPath . '.table.td')
                        {{ trans('way2translate::translation.status-display.' . $translatedLocale['active']) }}
                    @endcomponent
                    
                    @component($themeComponentPath . '.table.td')
                        @component($themeComponentPath . '.buttons.primary')
                            @slot('href', route('way2translate.group.index', $translatedLocale['code']))
                            @slot('small', true)

                            {{ trans('way2translate::action.edit') }}
                        @endcomponent

                        @if ($translatedLocale['code'] != Config::get('way2translate.default-locale'))
                            @if ($translatedLocale['active'])
                                @component($themeComponentPath . '.buttons.disable')
                                    @slot('href', route('way2translate.deactivate', $translatedLocale['code']))
                                    @slot('small', true)

                                    {{ trans('way2translate::action.deactivate') }}
                                @endcomponent
                            @else
                                @component($themeComponentPath . '.buttons.enable')
                                    @slot('href', route('way2translate.activate', $translatedLocale['code']))
                                    @slot('small', true)

                                    {{ trans('way2translate::action.activate') }}
                                @endcomponent
                            @endif
                        @endif
                    @endcomponent
                @endcomponent
            @endforeach
        @endcomponent
    @endcomponent
@endsection

@section('new-language-heading', trans('way2translate::translation.new-language'))

@section('new-language-form')
    {!! Form::open([
        'id'     => 'add-language',
        'method' => 'post',
        'url'    => route('way2translate.add'),
        'class'  => config('way2translate.theme.classes.form')
    ]) !!}
        @component($themeComponentPath . '.form.group')
            {{ Form::select('locale_target', $nonTranslatedLocalesTarget, null, [
                'class' => config('way2translate.theme.classes.select')
            ]) }}
            
            @if ($errors->has('locale_target'))
                @component($themeComponentPath . '.form.error')
                    {{ $errors->first('locale_target') }}
                @endcomponent
            @endif
        @endcomponent

        @component($themeComponentPath . '.form.group')
            @component($themeComponentPath . '.buttons.submit')
                @slot('form', 'add-language')

                {{ trans('way2translate::action.add-language') }}
            @endcomponent
        @endcomponent
    {{ Form::close() }}
@endsection
