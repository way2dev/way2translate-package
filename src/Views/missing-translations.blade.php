@extends($themePath . '.pages.error')

@section('title', trans('way2translate::page.missing-translations'))
@section('heading', trans('way2translate::page.missing-translations'))

@section('text')
    {{ trans('way2translate::translation.missing-translations') }}
@endsection
