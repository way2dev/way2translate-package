<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;

/**
 * Add a table to store activated translations.
 */
class AddWay2translateActivatedLocalesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('way2translate_activated_locales', function (Blueprint $table) {
            $table->string('locale')->unique()->index();
            $table->timestamps();
        });

        // add default locale
        DB::table('way2translate_activated_locales')->insert([
            'locale'     => Config::get('way2translate.default-locale'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('way2translate_activated_locales');
    }
}
