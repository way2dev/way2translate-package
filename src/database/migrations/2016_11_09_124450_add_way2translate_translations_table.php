<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Add the translations table.
 */
class AddWay2translateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('way2translate_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale', 10);
            $table->string('namespace', 150)->default('*');
            $table->string('group', 150);
            $table->string('name', 150);
            $table->text('value');
            $table->boolean('in_latest_import');
            $table->timestamps();
            $table->unique(['locale', 'namespace', 'group', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('way2translate_translations');
    }
}
