<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Adds an id column to the Way2Translate languages table.
 */
class AddIdColumnToWay2translateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('way2translate_languages', function (Blueprint $table) {
            $table->dropPrimary(['locale']);
        });

        Schema::table('way2translate_languages', function (Blueprint $table) {
            $table->increments('id')->first();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('way2translate_languages', function (Blueprint $table) {
            $table->dropColumn(['id']);
            $table->primary('locale');
        });
    }
}
