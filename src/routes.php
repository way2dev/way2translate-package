<?php

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
    ],
    function () {
        Route::group(
            [
                'prefix'     => 'way2translate',
                'as'         => 'way2translate.',
                'middleware' => ['web', 'auth', 'role:admin'],
            ],
            function () {
                Route::middleware('non-editable-languages')->group(function () {
                    Route::get('/index', [
                        'as'   => 'index',
                        'uses' => '\Way2Web\Way2Translate\Controllers\TranslationsController@index',
                    ]);

                    Route::get('/activate/{localeCode}', [
                        'as'   => 'activate',
                        'uses' => '\Way2Web\Way2Translate\Controllers\TranslationsController@activate',
                    ]);

                    Route::get('/deactivate/{localeCode}', [
                        'as'   => 'deactivate',
                        'uses' => '\Way2Web\Way2Translate\Controllers\TranslationsController@deactivate',
                    ]);

                    Route::post('/add', [
                        'as'   => 'add',
                        'uses' => '\Way2Web\Way2Translate\Controllers\TranslationsController@add',
                    ]);
                });

                Route::get('/missing-translations', [
                    'as'   => 'missing-translations',
                    'uses' => '\Way2Web\Way2Translate\Controllers\TranslationsController@missingTranslations',
                ]);

                Route::group(
                    [
                        'prefix'     => 'group/{localeCode}',
                        'as'         => 'group.',
                    ],
                    function () {
                        Route::get('/{group?}', [
                            'as'   => 'index',
                            'uses' => '\Way2Web\Way2Translate\Controllers\TranslationsController@group',
                        ]);

                        Route::post('/{group}', [
                            'as'   => 'save',
                            'uses' => '\Way2Web\Way2Translate\Controllers\TranslationsController@groupSave',
                        ]);
                    }
                );
            }
        );
    }
);
